package com.nv.sberschool.db;

import com.nv.sberschool.db.dao.BookDaoBean;
import com.nv.sberschool.db.model.Book;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

import static com.nv.sberschool.db.constants.DBConstants.*;

@SpringBootApplication
//@Import(MyDBConfigContext.class)
public class Application implements CommandLineRunner {

    Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    private BookDaoBean bookDaoBean;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        bookDaoBean.findBookById(2);
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerName(DB_HOST);
        dataSource.setDatabaseName(DB);
        dataSource.setPortNumber(Integer.valueOf(PORT));
        dataSource.setUser(USER);
        dataSource.setPassword(PASSWORD);
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        List<Book> books = jdbcTemplate.query(
                "select * from public.books",
                (resultSet, rowNum) -> {
                    Book book = new Book();
                    book.setId(resultSet.getInt("id"));
                    book.setTitle(resultSet.getString("title"));
                    book.setAuthor(resultSet.getString("author"));
                    book.setDateAdded(resultSet
                            .getTimestamp("date_added")
                            .toLocalDateTime());
                    return book;
                });
        books.forEach(b -> log.info(b.toString()));
    }
//    public static void main(String[] args) {
//        ApplicationContext ctx =
//                new AnnotationConfigApplicationContext(MyDBConfigContext.class);
//
//        BookDaoBean bookDaoBean = ctx.getBean(BookDaoBean.class);
//        bookDaoBean.findBookById(2);
//    }
}
