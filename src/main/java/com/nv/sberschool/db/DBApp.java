package com.nv.sberschool.db;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBApp {
    private static final DBApp INSTANCE = new DBApp();

    public static DBApp getInstance() {
        return INSTANCE;
    }

    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection(
                        "jdbc:postgresql://localhost:5432/local_db",
                        "postgres", "12345");
    }
}
