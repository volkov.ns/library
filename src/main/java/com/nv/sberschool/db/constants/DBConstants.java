package com.nv.sberschool.db.constants;

public class DBConstants {
    public static final String DB_HOST = "localhost";
    public static final String DB = "local_db";
    public static final String USER = "postgres";
    public static final String PASSWORD = "12345";
    public static final String PORT = "5432";
}
