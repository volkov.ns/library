package com.nv.sberschool.db.dao;

import com.nv.sberschool.db.DBApp;
import com.nv.sberschool.db.model.Book;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;

public class BookDaoJdbc {
    private static final Logger logger = LoggerFactory.getLogger(BookDaoJdbc.class);

    private PGConnectionPoolDataSource dataSource;

    public BookDaoJdbc(PGConnectionPoolDataSource dataSource) {
        this.dataSource = dataSource;
    }

    //select * from public.books where id = ?;
    public Book findBookById(Integer id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement selectQuery = connection.prepareStatement(
                    "select * from public.books where id=?");
            selectQuery.setInt(1, id);
            ResultSet resultSet = selectQuery.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(Integer.valueOf(resultSet.getString("id")));
                book.setTitle(resultSet.getString("title"));
                book.setAuthor(resultSet.getString("author"));
                book.setDateAdded(resultSet
                        .getTimestamp("date_added")
                        .toLocalDateTime());
                logger.info("Find book by id={}:{}", id, book);
                return book;
            }
        } catch (SQLException e) {
            logger.error("Unexpected exception", e);
        }
        return null;
    }
}
