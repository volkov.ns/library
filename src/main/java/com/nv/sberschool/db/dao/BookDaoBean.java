package com.nv.sberschool.db.dao;

import com.nv.sberschool.db.model.Book;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDaoBean {
    private static final Logger logger = LoggerFactory.getLogger(BookDaoJdbc.class);

    private final Connection connection;

    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }

    //select * from public.books where id = ?;
    public Book findBookById(Integer id) {
        try {
            PreparedStatement selectQuery = connection.prepareStatement(
                    "select * from public.books where id=?");
            selectQuery.setInt(1, id);
            ResultSet resultSet = selectQuery.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(Integer.valueOf(resultSet.getString("id")));
                book.setTitle(resultSet.getString("title"));
                book.setAuthor(resultSet.getString("author"));
                book.setDateAdded(resultSet
                        .getTimestamp("date_added")
                        .toLocalDateTime());
                logger.info("Find book by id={}:{}", id, book);
                return book;
            }
        } catch (SQLException e) {
            logger.error("Unexpected exception", e);
        }
        return null;
    }
}
